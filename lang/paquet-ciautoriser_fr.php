<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'ciautoriser_description' => 'Offre un pipeline qui permet à plusieurs plugins de modifier la même autorisation.',
	'cioidc_slogan' => 'Pipeline pour certaines fonctions de autoriser.'
);

?>