# Plugin CIAUTORISER

Il offre un véritable pipeline qui permet à plusieurs plugins de modifier la même autorisation.


## Les objectifs de ce plugin

Si un plugin doit intervenir sur les autorisations de voir les rubriques, il peut charger, via le pipeline "autoriser", sa propre fonction "autoriser_rubrique_voir".

Si un second plugin doit également intervenir, de manière différente, sur les autorisations de voir les rubriques, il peut charger, via le pipeline "autoriser", sa propre fonction "autoriser_rubrique_voir".

Toutefois, la fonction "autoriser_rubrique_voir" figurera dans les deux plugins et PHP considère comme erreur fatale le fait de déclarer deux fois la même fonction.

On peut vérifier l'existence de la fonction préalablement, mais dans ce cas une seule fonction "autoriser_rubrique_voir" pourra être utilisée et l'un des plugin restera sans effet sur les autorisations de voir les rubriques.

L'objectif du plugin "ciautoriser" est d'offrir un véritable pipeline qui permette à plusieurs plugins de modifier la même autorisation.


## Documentation

[Documentation](https://contrib.spip.net/ciautoriser-plugin-Pipeline-pour-autoriser)


## Compatibilité

Le plugin est compatible avec : 
- SPIP 3.2, 4.0, 4.1, 4.2, 4.3.
- PHP 7.4, 8.0, 8.1, 8.2, 8.3.


## Installation

Le plugin s'installe comme tous les plugins.
